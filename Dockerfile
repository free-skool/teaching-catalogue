FROM python:3
ENV PYTHONUNBUFFERED 1
ENV SETTINGS_ENV "dev"
#ENV USE_POSTGRESQL 1

RUN apt-get update && apt-get install -y gettext
RUN mkdir /code

WORKDIR /code

COPY requirements.txt /code/
RUN python3 -m venv env
RUN env/bin/pip install --no-cache-dir -r requirements.txt

COPY docker/* /usr/local/bin/
COPY .coveragerc /code/

WORKDIR /code/src
ENTRYPOINT ["entrypoint.sh"]
