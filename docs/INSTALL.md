# Installation

## Prerequistes

- Install postgresql
- Use a dedicated user for your django app

## Debian / ubuntu install

(In progress)

Install debian dependencies

```
$ sudo apt install curl gettext git build-essential python3 python3-pip python3-venv
```

Install postgresql

```
apt install postgresql-11
pg_ctlcluster 11 main start
```

Create user

```
adduser djuser
```

Clone repository and install python dependencies

```
python3 -m venv venv && source venv/bin/activate
git clone https://framagit.org/free-skool/teaching-catalogue.git
cd teaching-catalogue/
pip install -r requirements.txt
```

Deploy django app

```
source ../venv/bin/activate
./bin/init.sh
```

## Docker install

Only for dev purpose, use sqlite3 db connector.

```
docker-compose build
docker-compose run --rm web init.sh
docker-compose up -d
```

Run tests

```
docker-compose run --rm web /code/env/bin/python ./manage.py test
```

## On Gandi Simple hosting

- create a new instance with python3 and postgresql 11
- create a site in your new instance
- create a new db and a new role
- prepare local.py settings with your db credentials
- git push tout ça
- send local.py via ftp (ignored by git)
- activate console and execute

```bash
./bin/init.sh
```
