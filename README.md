# TP Atelier Django

## Install

### Install python3 and _Virtualenv_

```!console
$ sudo apt install python3 curl git build-essential sqlite \
python3 \
python3-venv
$ python3 -m venv venv
```

### Install Django & Django REST framework

```!console
(venv) $ pip install django djangorestframework
```

## Run server

```!console
(venv) $ ./manage.py runserver
```
