# Teaching resources catalog

[![pipeline status](https://framagit.org/free-skool/teaching-catalogue/badges/master/pipeline.svg)](https://framagit.org/free-skool/teaching-catalogue/-/commits/master)
[![coverage report](https://framagit.org/free-skool/teaching-catalogue/badges/master/coverage.svg)](https://framagit.org/free-skool/teaching-catalogue/-/commits/master)

This django app lists resources, courses material and other pedagogic contents and their authors, with level, topics and licenses info.

Here is the [original idea coming to that app](http://potcommun.adn56.net/) and [some wireframes](https://framagit.org/numahell/upload-proto/blob/master/design/upload.pdf).

It is deployed in [ressources.educpopnum.net](https://ressources.educpopnum.net/)

## Snapshots

![Resources list](./docs/screenshots/teaching-catalog-resource-list.png)

![Organizations list](./docs/screenshots/teaching-catalog-organization-list.png)

![Add a resource](./docs/screenshots/teaching-catalog-add-resource.png)

## Features

* Visitors can see resources and organizations list
* Visitors can search features on title, description, tags and published status
* Visitors can submit a new resource
* Moderators can moderate resources
* Resources has tags, levels, author, organization fields
* Admin can load licenses from spdx
* API read only available for resources and organizations

## Install

See [install documentation](./docs/INSTALL.md)

## Roadmap

### Version 0.4

* [x] Search feature
* [x] Improve API
* [x] French translations

### Version 0.5

* [ ] Add post for API
* [ ] Allow registration (add email backend)
* [ ] Improve users and permissions
* [ ] Organization map

### "Plan sur la comète"

- Categories or format
- Moderation panel
- Comment and validation
- Abstract models
