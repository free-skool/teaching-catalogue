STATIC_ROOT = '/var/www/teaching-catalogue/static/'

DATABASES = {
    'default': {
       'ENGINE': 'django.db.backends.postgresql_psycopg2',
       'NAME': 'database_ci',
       'USER': 'postgres',
       'PASSWORD': 'postgres',
       'HOST': 'postgres',
       'PORT': '5432',
    },
}

SITE_TITLE = "Ressources pédagogiques pour …"
