from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.utils.translation import gettext as _


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(
        max_length=100,
        required=False,
        label=_('Display Name'),
        help_text=_('Optional.')
    )
    email = forms.EmailField(
        max_length=254,
        label=_('Email'),
        help_text=_('Required. Inform a valid email address.')
    )

    class Meta:
        model = User
        fields = ('username', 'first_name', 'email', 'password1', 'password2',)
