import os
from unittest import mock

from django.conf import settings
from django.test import TestCase

from teaching import helpers
from teaching.tests.utils import FakeResponse


class MetaFromUrlTests(TestCase):
    """Test homepage display"""

    @mock.patch('requests.get')
    def test_get_meta_from_url(self, mock_get):
        # Mock response
        testfile = os.path.join(os.path.dirname(settings.BASE_DIR), 'teaching/tests/data/index.html')
        mock_response = FakeResponse(testfile)
        mock_get.return_value = mock_response

        metas = helpers.get_meta_from_url('https://example.com/')
        self.assertEqual(metas['title'], 'Framasoft')
        self.assertEqual(metas['description'], 'Framasoft, c’est une association d’éducation populaire…')

    @mock.patch('requests.get')
    def test_get_meta_from_url_notog(self, mock_get):
        # Mock response
        testfile = os.path.join(os.path.dirname(settings.BASE_DIR), 'teaching/tests/data/index_noog.html')
        mock_response = FakeResponse(testfile)
        mock_get.return_value = mock_response

        metas = helpers.get_meta_from_url('https://example.com/')
        self.assertEqual(metas['title'], 'Framasoft')
        self.assertEqual(metas['description'], 'Framasoft, c’est une association d’éducation populaire…')

    @mock.patch('requests.get')
    def test_get_meta_from_url_noog_notwitter(self, mock_get):
        # Mock response
        testfile = os.path.join(os.path.dirname(settings.BASE_DIR), 'teaching/tests/data/index_noog_notwitter.html')
        mock_response = FakeResponse(testfile)
        mock_get.return_value = mock_response

        metas = helpers.get_meta_from_url('https://example.com/')
        self.assertEqual(metas['title'], 'Framasoft')
        self.assertEqual(metas['description'], 'Framasoft, c’est une association d’éducation populaire…')
