import os
from django.conf import settings


class FakeResponse:
    """Build fake response, status_code 200, content from a file"""

    status_code = 200
    content = b""

    def __init__(self, content_filename=None):
        if not content_filename:
            content_filename = os.path.join(os.path.dirname(settings.BASE_DIR), 'teaching/tests/data/index.html')
        with open(content_filename, 'r') as f:
            self.content = f.read()
