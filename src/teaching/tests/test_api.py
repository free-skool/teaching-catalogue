from django.contrib.auth.models import User
from django.test import TestCase
from django.test.utils import override_settings
from teaching.models import PublicationStatus
from teaching.factories import CourseMaterialFactory, OrganizationFactory, TagFactory, AuthorFactory


class CourseMaterialAPITests(TestCase):

    def setUp(self):
        self.course1 = CourseMaterialFactory()
        self.user1 = User.objects.create_user('Louise Michel', 'ook')

    def test_anon_list(self):
        response = self.client.get('/api/course_material/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.course1.title)

    def test_anon_retrieve(self):
        response = self.client.get(f'/api/course_material/1/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.get('title'), self.course1.title)

    def test_anon_cannot_create(self):
        """Anon don't have write permission"""
        response = self.client.post('/api/course_material/',
                                    data={'title': 'My new course'})
        self.assertEqual(response.status_code, 403)

    def test_authenticated_create(self):
        """Post is not configured"""
        self.client.force_login(self.user1)
        response = self.client.post('/api/course_material/',
                                    data={'title': 'My new course'})
        self.assertEqual(response.status_code, 405)


class OrganizationAPITests(TestCase):

    def setUp(self):
        self.orga1 = OrganizationFactory()
        self.user1 = User.objects.create_user('Louise Michel', 'ook')

    def test_anon_list(self):
        response = self.client.get('/api/organization/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.orga1.name)

    def test_anon_retrieve(self):
        response = self.client.get(f'/api/organization/1/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.get('name'), self.orga1.name)

    def test_anon_cannot_create(self):
        """Anon don't have write permission"""
        response = self.client.post('/api/organization/',
                                    data={'name': 'My new orga'})
        self.assertEqual(response.status_code, 403)

    def test_authenticated_create(self):
        """Post is not configured"""
        self.client.force_login(self.user1)
        response = self.client.post('/api/organization/',
                                    data={'name': 'My new orga'})
        self.assertEqual(response.status_code, 405)
