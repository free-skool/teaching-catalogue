import os
from unittest import mock

from django.conf import settings
from django.test import TestCase
from django.test.utils import override_settings

from teaching.models import PublicationStatus
from teaching.factories import CourseMaterialFactory, OrganizationFactory, TagFactory, AuthorFactory, FormatFactory
from teaching.tests.utils import FakeResponse


class HomepageTests(TestCase):
    """Test homepage display"""

    def test_homepage_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    @override_settings(SITE_TITLE='My website')
    def test_title_site_in_settings(self):
        response = self.client.get('/')
        self.assertEqual(response.context.get('homepage_title'), 'My website')

    @override_settings(SITE_TITLE=None, SITE_ID=1)
    def test_title_site_in_site(self):
        response = self.client.get('/')
        self.assertEqual(response.context.get('homepage_title'), 'example.com')


class CourseMaterialTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.tag1 = TagFactory()
        cls.tag2 = TagFactory()
        cls.author1 = AuthorFactory()
        cls.author2 = AuthorFactory()
        cls.course1 = CourseMaterialFactory(
            status=PublicationStatus.PUBLISHED,
            tags=(cls.tag1, cls.tag2),
            contributors=(cls.author1, cls.author2),
            description=("Super guide\n\n- pour débuter\n- pour continuer"),
        )
        cls.course2 = CourseMaterialFactory()

    def test_material_list(self):
        response = self.client.get('/coursematerial', HTTP_ACCEPT_LANGUAGE='en')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.course1.title)
        self.assertContains(response, self.course1.resource_license)
        self.assertContains(response, self.course1.level.label)
        self.assertContains(response, self.course1.tags.first().label)
        self.assertContains(response, 'Add a resource')

    @mock.patch('requests.get')
    def test_add_course_from_url(self, mock_get):
        # Mock response
        testfile = os.path.join(os.path.dirname(settings.BASE_DIR), 'teaching/tests/data/index.html')
        mock_response = FakeResponse(testfile)
        mock_get.return_value = mock_response

        response = self.client.get('/coursematerial/add?page_url=https://framasoft.org/')
        initial_data = response.context_data['form'].initial
        self.assertEqual(initial_data.get('course_url'), 'https://framasoft.org/')
        self.assertEqual(initial_data.get('title'), 'Framasoft')
        self.assertEqual(initial_data.get('description'), "Framasoft, c’est une association d’éducation populaire…")

    def test_add_course_page_url_malformatted(self):
        response = self.client.get('/coursematerial/add?page_url=')
        self.assertNotIn('course_url', response.context_data['form'].initial)

    def test_material_pending_list(self):
        response = self.client.get('/coursematerial/pending', HTTP_ACCEPT_LANGUAGE='en')
        self.assertContains(response, 'Pending course material list')
        self.assertContains(response, '{0}'.format(self.course2.title))

    def test_material_detail(self):
        response = self.client.get('/coursematerial/{0}'.format(self.course1.pk))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.course1.title)
        self.assertContains(response, self.course1.resource_license)
        self.assertContains(response, self.course1.format)
        self.assertContains(response, self.course1.author.name)
        self.assertContains(response, self.course1.contributors.first().name)
        self.assertContains(response, self.course1.level.label)
        self.assertContains(response, self.course1.tags.first().label)
        self.assertContains(response, "<li>pour débuter</li>", html=True)


class CourseMaterialSearch(TestCase):
    """Search form tests on course material view"""

    def setUp(self):
        self.tag1 = TagFactory(label='Graphisme')
        self.tag2 = TagFactory(label='Python')
        self.tag3 = TagFactory(label='Programmation')
        self.format1 = FormatFactory(label='Tutoriel')
        self.course1 = CourseMaterialFactory.create(
            title='Initiation à Gimp',
            description='Gimp, retouche photo, détourage…',
            status=PublicationStatus.PUBLISHED,
            tags=(self.tag1.pk,),
            format=self.format1,
        )
        self.course2 = CourseMaterialFactory.create(
            title='Initiation à Inkscape',
            description='Inkscape, dessin vectoriel, créez vos flyers',
            status=PublicationStatus.PUBLISHED,
            tags=(self.tag1.pk,),
        )
        self.course3 = CourseMaterialFactory.create(
            title='Python, concepts avancés',
            description='Langage de programmation python',
            tags=(self.tag2.pk, self.tag3.pk),
        )

    def test_no_search_published_only(self):
        response = self.client.get('/coursematerial')
        self.assertContains(response, self.course1.title)
        self.assertContains(response, self.course2.title)
        self.assertNotContains(response, self.course3.title)

    def test_search_text_in_title(self):
        response = self.client.get('/coursematerial?q_term=gimp')
        self.assertContains(response, self.course1.title)
        self.assertNotContains(response, self.course2.title)

    def test_search_text_in_description(self):
        response = self.client.get('/coursematerial?q_term=flyers')
        self.assertNotContains(response, self.course1.title)
        self.assertContains(response, self.course2.title)

    def test_search_published(self):
        response = self.client.get('/coursematerial?q_published=on')
        self.assertContains(response, self.course1.title)
        self.assertNotContains(response, self.course3.title)

    def test_search_tags(self):
        response = self.client.get('/coursematerial?q_tags={0}'.format(self.tag1.pk))
        self.assertContains(response, self.course1.title)
        self.assertNotContains(response, self.course3.title)

    def test_search_format(self):
        response = self.client.get('/coursematerial?q_format={0}'.format(self.format1.pk))
        self.assertContains(response, self.course1.title)
        self.assertNotContains(response, self.course2.title)
        self.assertNotContains(response, self.course3.title)


class OrganizationTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.orga1 = OrganizationFactory()

    def test_organization_list(self):
        response = self.client.get('/organization')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.orga1.name)
        self.assertContains(response, self.orga1.website)
        self.assertContains(response, self.orga1.city)
        self.assertNotContains(response, 'Ajouter une organisation')

    def test_add_organization_onlyloggued(self):
        response = self.client.get('/organization/add', follow=True)
        self.assertEqual(response.request['PATH_INFO'], '/accounts/login/')
