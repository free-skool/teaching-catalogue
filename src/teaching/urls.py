from django.urls import path, include
from django.views.generic import TemplateView
from rest_framework import routers
from . import views, api_views


router = routers.DefaultRouter()
router.register(r'course_material', api_views.CourseMaterialViewSet)
router.register(r'organization', api_views.OrganizationViewSet)


urlpatterns = [
    path('', views.LastCourseList.as_view(), name='index'),
    path('coursematerial', views.CourseMaterialList.as_view(), name='coursematerial-list'),
    path('coursematerial/pending', views.CourseMaterialPendingList.as_view(), name='coursematerial-pending-list'),
    path('coursematerial/<int:pk>', views.CourseMaterialDetail.as_view(), name='coursematerial-detail'),
    path('coursematerial/add', views.CourseMaterialCreateView.as_view(), name='coursematerial-create'),
    path('coursematerial/thanks', TemplateView.as_view(template_name="teaching/thanx.html"), name='coursematerial-thanx'),
    path('coursematerial/<int:pk>/edit', views.CourseMaterialUpdateView.as_view(), name='coursematerial-update'),
    path('coursematerial/<int:pk>/changestatus', views.CourseMaterialChangeStatusView.as_view(), name='coursematerial-change-status'),
    path('organization', views.OrganizationList.as_view(), name='organization-list'),
    path('organization/add', views.OrganizationCreateView.as_view(), name='organization-create'),
    path('organization/thanks', TemplateView.as_view(template_name="teaching/thanx.html"), name='organization-thanx'),
    path('organization/<int:pk>/edit', views.OrganizationUpdateView.as_view(), name='organization-update'),
]

urlpatterns += path('api/', include((router.urls, 'teaching'))),
