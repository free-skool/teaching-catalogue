import json
from urllib import request

from django.core.management.base import BaseCommand

from teaching.models import License

SPDX_LICENSES_JSON = "https://raw.githubusercontent.com/spdx/license-list-data/master/json/licenses.json"


class Command(BaseCommand):
    help = "Import licenses from spdx"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        """Handle command"""
        r = request.urlopen(SPDX_LICENSES_JSON)
        data = json.loads(r.read().decode(r.info().get_param('charset') or 'utf-8'))
        
        for license in data['licenses']:
            the_license, created = License.objects.update_or_create(
                code=license['licenseId'].lower(),
                title=license['licenseId'],
                description=license['name']
            )
            if 'seeAlso' in license.keys():
                the_license.url = license['seeAlso'][0]
            the_license.save()
