from rest_framework import viewsets, permissions
from . import models, serializers


class CourseMaterialViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.CourseMaterial.objects.all()
    serializer_class = serializers.CourseMaterialSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class OrganizationViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Organization.objects.all()
    serializer_class = serializers.OrganizationSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]