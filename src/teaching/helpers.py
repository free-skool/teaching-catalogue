import requests
from bs4 import BeautifulSoup


def get_meta_from_url(page_url):
    useful_meta = {}
    response = requests.get(page_url)
    soup = BeautifulSoup(response.content, features="html.parser")

    # Find title
    title = ""
    if soup.find(attrs={"property": "og:title"}):
        title = soup.find(attrs={"property": "og:title"}).get('content')
    elif soup.find(attrs={"name": "twitter:title"}):
        title = soup.find(attrs={"name": "twitter:title"}).get('content')
    elif soup.title:
        title = soup.title.string
    useful_meta.update({'title': title})

    # Find description
    description = ""
    if soup.find(attrs={"property": "og:description"}):
        description = soup.find(attrs={"property": "og:description"}).get('content')
    elif soup.find(attrs={"name": "twitter:description"}):
        description = soup.find(attrs={"name": "twitter:description"}).get('content')
    elif soup.find(attrs={"name": "description"}):
        description = soup.find(attrs={"name": "description"}).get('content')
    useful_meta.update({'description': description})

    return useful_meta
