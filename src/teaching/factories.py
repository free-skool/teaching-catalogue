from urllib3.util import parse_url
import factory
from factory.django import DjangoModelFactory
from teaching import models


class AuthorFactory(DjangoModelFactory):
    class Meta:
        model = models.Author
    name = factory.Faker('name')


class LicenseFactory(DjangoModelFactory):
    class Meta:
        model = models.License
    code = factory.Sequence(lambda n: 'license-{0}'.format(n))
    title = factory.Sequence(lambda n: 'License-{0}'.format(n))


LEVELS = ['Beginner', 'Intermediate', 'Advanced']


class LevelFactory(DjangoModelFactory):
    class Meta:
        model = models.Level
    label = factory.Faker('text', max_nb_chars=10)
    description = factory.Faker('text', max_nb_chars=255)


class FormatFactory(DjangoModelFactory):
    class Meta:
        model = models.Format
    label = factory.Faker('text', max_nb_chars=10)
    description = factory.Faker('text', max_nb_chars=255)


class TagFactory(DjangoModelFactory):
    class Meta:
        model = models.Tag
    label = factory.Faker('text', max_nb_chars=10)
    description = factory.Faker('text', max_nb_chars=255)


class OrganizationFactory(DjangoModelFactory):
    class Meta:
        model = models.Organization
    name = factory.Faker('text', max_nb_chars=15)
    description = factory.Faker('text', max_nb_chars=255)
    website = factory.Faker('url')
    address = factory.Faker('address')
    city = factory.Faker('city')

    @factory.lazy_attribute
    def contact_email(self):
        domain = parse_url(self.website).host
        contact_email = 'contact@{0}'.format(domain)


class CourseMaterialFactory(DjangoModelFactory):
    class Meta:
        model = models.CourseMaterial
    title = factory.Faker('text', max_nb_chars=25)
    course_url = factory.Faker('url')
    author = factory.SubFactory(AuthorFactory)
    level = factory.SubFactory(LevelFactory)
    format = factory.SubFactory(FormatFactory)
    resource_license = factory.SubFactory(LicenseFactory)
    organization = factory.SubFactory(OrganizationFactory)

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for tag in extracted:
                self.tags.add(tag)

    @factory.post_generation
    def contributors(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for contributor in extracted:
                self.contributors.add(contributor)
