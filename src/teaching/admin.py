from django.contrib import admin
from teaching import models


class LicenseAdmin(admin.ModelAdmin):
    list_display = ('title', 'description')
    search_fields = ('title',)
    ordering = ('title',)


class TagAdmin(admin.ModelAdmin):
    list_display = ('label', 'description')
    search_fields = ('label',)
    ordering = ('label',)


class FormatAdmin(admin.ModelAdmin):
    list_display = ('label', 'description')
    search_fields = ('label',)
    ordering = ('label',)


class CourseMaterialAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'resource_license', 'level', 'status')
    ordering = ('title',)
    search_fields = ('title', 'description')
    list_filter = ('status', 'level', 'tags')
    autocomplete_fields = ['resource_license', 'tags']


admin.site.register(models.License, LicenseAdmin)
admin.site.register(models.Author)
admin.site.register(models.Organization)
admin.site.register(models.Level)
admin.site.register(models.Format, FormatAdmin)
admin.site.register(models.Tag, TagAdmin)
admin.site.register(models.CourseMaterial, CourseMaterialAdmin)
