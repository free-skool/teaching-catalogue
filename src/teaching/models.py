from django.db import models
from django.utils.translation import gettext as _


class LabelDescriptionModelMixin(models.Model):
    """Label Description model mixin"""
    label = models.CharField(_('Label'), max_length=200, blank=True, null=True)
    description = models.TextField(_("Description"), blank=True, null=True)

    class Meta:
        abstract = True
        ordering = ['label']

    def __str__(self):
        return self.label


class PublicationStatus(models.TextChoices):
    """Publication status choices"""
    DRAFT = 'DRAFT', _('Draft')
    PENDING = 'PENDING', _('Pending')
    PUBLISHED = 'PUBLISHED', _('Published')
    REFUSED = 'REFUSED', _('Refused')


class Author(models.Model):
    """Model for Author"""
    name = models.CharField(_('Name'), max_length=100)

    class Meta:
        verbose_name = _('author')
        verbose_name_plural = _('authors')
        ordering = ['name']

    def __str__(self):
        return self.name


class Organization(models.Model):
    """Model for organization"""
    name = models.CharField(_('Name'), max_length=255)
    description = models.TextField(_('Description'), blank=True, null=True, help_text=_("You can use markdown"))
    website = models.URLField(_('Website'), max_length=200, blank=True, null=True)
    address = models.CharField(_('Address'), max_length=255, blank=True, null=True)
    city = models.CharField(_('City'), max_length=255, blank=True, null=True)
    contact_email = models.EmailField(_('Email'), blank=True, null=True)

    class Meta:
        verbose_name = _('Organization')
        verbose_name_plural = _('Organizations')
        ordering = ['name']

    def __str__(self):
        return self.name


class License(models.Model):
    """Model for License"""
    code = models.CharField(_('Code'), max_length=80)
    title = models.CharField(_('Title'), max_length=200, blank=True, null=True)
    description = models.TextField(_("Description"), blank=True, null=True)
    url = models.URLField('URL', max_length=200, blank=True, null=True)

    class Meta:
        verbose_name = _('license')
        verbose_name_plural = _('license')
        ordering = ["title"]

    def __str__(self):
        return self.title


class Level(LabelDescriptionModelMixin):
    """Model for Level"""

    class Meta:
        verbose_name = _('level')
        verbose_name_plural = _('levels')


class Tag(LabelDescriptionModelMixin):
    """Model for Tag"""

    class Meta:
        verbose_name = _('tag')
        verbose_name_plural = _('tags')


class Format(LabelDescriptionModelMixin):
    """Model for Course format"""

    class Meta:
        verbose_name = _('format')
        verbose_name_plural = _('formats')


class CourseMaterial(models.Model):
    """Model for CourseMaterial"""

    title = models.CharField(_('Title'), max_length=100)
    description = models.TextField(
        _('Description'),
        max_length=500, blank=True, null=True,
        help_text=_("You can use markdown")
    )
    tags = models.ManyToManyField(
        Tag,
        blank=True,
        related_name="course_materials"
    )
    format = models.ForeignKey(
        Format,
        verbose_name=_('Format'),
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    status = models.CharField(
        _('Status'),
        max_length=10,
        choices=PublicationStatus.choices,
        default=PublicationStatus.PENDING
    )
    level = models.ForeignKey(
        Level,
        verbose_name=_('Level'),
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    author = models.ForeignKey(
        Author,
        verbose_name=_('Author'),
        related_name='courses',
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    contributors = models.ManyToManyField(
        Author,
        verbose_name=_('Contributor'),
        related_name='contributed_courses',
        blank=True
    )
    organization = models.ForeignKey(
        Organization,
        verbose_name=_('Training organization'),
        related_name='courses',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    resource_license = models.ForeignKey(
        License,
        verbose_name=_('Resource License'),
        related_name='courses',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    course_url = models.URLField(
        'URL',
        max_length=200, blank=True, null=True
    )

    class Meta:
        verbose_name = _('course material')
        verbose_name_plural = _('course materials')
        ordering = ['title']
        permissions = [
            ('can_publish_coursematerial', 'Can publish CourseMaterial'),
        ]

    def __str__(self):
        return self.title

    @property
    def is_published(self):
        return self.status == PublicationStatus.PUBLISHED
