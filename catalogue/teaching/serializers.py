from rest_framework import serializers
from . import models


class AuthorSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = models.Author
        fields = ['id', 'name', 'email']


class CourseMaterialSerializer(serializers.ModelSerializer):
    author = AuthorSerializer()

    class Meta:
        model = models.CourseMaterial
        fields = ['title', 'description', 'url', 'author']

    def create_or_update_author(self, author_data):
        try:
            author = models.Author.objects.get(id=author_data.get('id'))
            author.name = author_data['name']
            author.email = author_data['email']
        except models.Author.DoesNotExist:
            author = models.Author(**author_data)
        author.save()
        return author

    def create(self, validated_data):
        author = self.create_or_update_author(validated_data.pop('author'))
        course_material = models.CourseMaterial.objects.create(
            author=author,
            **validated_data,
        )
        return course_material

    def update(self, instance, validated_data):
        author = self.create_or_update_author(validated_data.pop('author'))
        instance.title = validated_data['title']
        instance.description = validated_data['description']
        instance.email = validated_data['email']
        instance.author = author
        instance.save()
        return instance
