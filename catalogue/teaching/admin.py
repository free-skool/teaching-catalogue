from django.contrib import admin
from teaching.models import Author, CourseMaterial

admin.site.register(Author)
admin.site.register(CourseMaterial)
