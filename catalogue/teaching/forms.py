from django import forms
from teaching.models import CourseMaterial


class CourseMaterialForm(forms.ModelForm):

    class Meta:
        model = CourseMaterial
        fields = ("title", "description", "url")
