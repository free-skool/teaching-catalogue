from django.urls import path, include
from rest_framework import routers
from . import views, api_views


router = routers.DefaultRouter()
router.register(r'course_material', api_views.CourseMaterialViewSet)


urlpatterns = [
    path('coursematerial', views.CourseMaterialList.as_view(), name='coursematerial-list'),
    path('coursematerial/<int:pk>', views.CourseMaterialDetail.as_view(), name='coursematerial-detail'),
    path('coursematerial/add', views.CourseMaterialCreateView.as_view(), name='coursematerial-create'),
]

urlpatterns += path('api/', include((router.urls, 'teaching'))),