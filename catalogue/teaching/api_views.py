from rest_framework import viewsets
from . import models, serializers


class CourseMaterialViewSet(viewsets.ModelViewSet):
    queryset = models.CourseMaterial.objects.all()
    serializer_class = serializers.CourseMaterialSerializer
    # permission_classes = [IsAccountAdminOrReadOnly]
