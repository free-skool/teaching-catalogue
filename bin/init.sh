#!/usr/bin/env bash
./bin/update.sh
cd src
./manage.py loaddata --app teaching ./teaching/fixtures/demo.json
./manage.py loaddata --app flatpages ./catalogue/fixtures/flatpages.json
cd -
